
Twig
****

Symfony utilise le moteur de rendu Twig. 

Ce moteur de rendu propose une syntaxe simple pour permettre aux designers de mettre en forme aisément du html. Il s'intègre parfaitement dans symfony, en donnant accès à certaines informations récupérées de symfony dans les templates, au moyen de variables.

Twig s'inspire du langage ``Jinja`` (`site web <http://jinja.pocoo.org/>`_) qui rencontre un grand succès dans le monde python.

.. seealso::

   La documentation twig pour les développeurs
      La documentation de twig propose `un résumé pour les développeurs <https://twig.sensiolabs.org/doc/2.x/templates.html>`_, qui a servi de base à cette page.


Insertion du code twig dans le html
===================================

Le code twig s'insère dans du html :

.. code-block:: html+jinja

   <!DOCTYPE html>
   <html>
       <head>
           <title>My Webpage</title>
       </head>
       <body>
           <ul id="navigation">
           {% for item in navigation %}
               <li><a href="{{ item.href }}">{{ item.caption }}</a></li>
           {% endfor %}
           </ul>

           <h1>My Webpage</h1>
           {{ a_variable }}
       </body>
   </html>

Le code entre ``{% ... %}`` insère des structures de controles (boucles, conditions (= ``if..else``, etc.).

Le code entre ``{{ ... }}`` imprime des résultats (contenu d'une variable, etc.).

Variables
=========

Le template twig reçoit des variables, généralement (mais pas uniquement) des controleurs. Voici comment imprimer la variable foo :

.. code-block:: html+jinja

   {{ foo }}

On peut également créer des variables dans les templates :

.. code-block:: html+jinja

   <!-- Créer une variable de type 'string' -->
   {% set foo = 'bar' %}

   <!-- Créer un tableau -->
   {% set list = [1, 2, 3, 4] %}

   <!-- Créer un tableau associatif -->
   {% set hash = { 'foo': 'bar', 'key': 'value' } %}

Twig permet d'avoir accès aux propriétés avec le ``.``. Exemple :

.. code-block:: html+jinja

   {{ foo.bar }}

Dans ce cas, 

- si ``foo`` est un tableau, il va chercher à afficher la clé "bar": ``$foo['bar']`` ;
- sinon, si ``foo`` est un objet, il va chercher la propriété publié "bar": ``$foo->bar`` ;
- sinon, twig vérifie si la fonction bar existe, et, dans ce cas, afficher son résultat: ``$foo->bar()`` ;
- sinon, twig va tester l'existence de la fonction (dans l'ordre) ``getBar()``, ``isBar()``, ``hasBar()``;
- sinon, twig va renvoyer une erreur.

.. tip::

   Dans le cas de tableau, la syntaxe ``{{ foo['bar'] }}`` va fonctionner également.

Les filtres
===========

Les filtres peuvent être utilisés pour modifier les variables. Exemple : 

.. code-block:: html+jinja

   <!-- mettre en majuscule -->
   {{ foo|uppercase }}
   
   <!-- joindre les éléments d'un tableau, en les séparant par une virgule -->
   {{ list|join(', ') }}

   <!-- compter le nombre  d'élément d'un tableau --> 
   {{ list|length }}


Twig apporte nativement `les filtres les plus courants <https://twig.sensiolabs.org/doc/2.x/filters/index.html>`_. Symfony en ajoute d'autres.

.. seealso::

   La liste complète des filtres
      https://twig.sensiolabs.org/doc/2.x/filters/index.html

   Les filtres ajoutés par symfony
      http://symfony.com/doc/current/reference/twig_reference.html#filters

Les fonctions
=============

Les fonctions génèrent du contenu. Exemple :

.. code-block:: html+jinja

   <!-- créer un tableau de 0 à 9 et l'assigner à foo -->
   {% set foo = range(0,9) %}

   <!-- créer un chemin relatif vers la route 'home' -->
   <a href="{{ path('home') }}">Mon lien</a>


Les structures de controles (boucle et conditions)
==================================================

``if``, ``elseif`` et ``else`` permettent d'indiquer des conditions :

.. code-block:: html+jinja

   {% if list|length = 0 %}
      Il n'y a aucun élément dans la liste.
   {% elseif list|length = 1 %}
      Il y a un élément dans la liste.
   {% else %}
      Il y a {{ list|length }} éléments dan la liste.
   {% endif %}

``for`` permet de créer des boucles :

.. code-block:: html+jinja 

   <ul>
   {% for value in list %}
      <li>{{ value }}</li>
   {% endfor %}
   </ul>

   <!-- en récupérant la clé du tableau -->
   <ul> 
   {% for key, value in list %}
      <li>{{ key }} : {{ value }}</li>
   {% endfor %}
   </ul>

Conditions
==========

Twig apporte un grande nombre de tests qui peuvent être utilisés dans les conditions :

.. code-block:: html+jinja

   <!-- Test que la variable commence par... -->
   {% if 'Julien' starts with 'J' %}
   {% endif %}

   {% if 1 in [1, 2, 3] %}
   <p>1 est contenu dans la liste<p>
   {% endif %}

Héritage de templates
=====================

L'héritage de template permet de construire un squelette "de base" qui contient tous les éléments du site, et de définir des "blocks" qui peuvent être surchargés par les templates *enfants*. 

Exemple avec un site en deux colonnes : 

.. code-block:: html+jinja

   <!DOCTYPE html>
   <html>
       <head>
           {% block head %}
               <link rel="stylesheet" href="style.css" />
               <title>{% block title %}{% endblock %} - My Webpage</title>
           {% endblock %}
       </head>
       <body>
           <div id="content">{% block content %}{% endblock %}</div>
           <div id="footer">
               {% block footer %}
                   &copy; Copyright 2011 by <a href="http://domain.invalid/">you</a>.
               {% endblock %}
           </div>
       </body>
   </html> 

Dans cet exemple, trois **blocks** sont définis :

- un block ``head``, qui contient lui-même un block ``title`` ;
- un block ``content``,
- et un block ``footer``

Une page enfant peut ressembler à ceci :

.. code-block:: html+jinja

   {% extends "base.html" %}

   {% block title %}Index{% endblock %}

   {% block head %}
    {{ parent() }}
    <style type="text/css">
        .important { color: #336699; }
    </style>
   {% endblock %}

   {% block content %}
    <h1>Index</h1>
    <p class="important">
        Welcome to my awesome homepage.
    </p>
   {% endblock %}

Ici, le tag ``extends`` indique que l'on surcharge le template de base.

Le template enfant surcharge les blocks ``title``, ``head`` et ``content`` avec son propre contenu. L'enfant peut ajouter le code du parent à l'aide de la fonction ``{{ parent() }}``.


