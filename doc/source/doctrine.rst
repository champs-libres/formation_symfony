
*************************
Doctrine ORM, l'essentiel
*************************

Doctrine est un projet d'ORM écrit en PHP. 

En tant qu'ORM, son rôle est de :

- transformer les objets PHP en écriture SQL ;
- récupérer les écritures SQL et les transformer en objet PHP;

Ainsi, 

- pour chaque classe, une table sera créé ;
- une propriété ``string`` dans PHP sera transformé en ``VARCHAR`` ou en ``TEXT`` dans une base de données SQL;
- une propriété ``float`` dans PHP deviendra un ``FLOAT`` ou ``DECIMAL`` en SQL ;
- une propriété ``\DateTime`` dans PHP sera transformé en ``DATETIME`` en SQL ;

.. figure:: _static/doctrine_mapping_single_entity.png

Doctrine gère également les associations: si un propriété contient d'autres classes, cette association sera refflétée dans le schéma SQL par une clé étrangère.

Schématiquement, cela donne ceci :

.. figure:: _static/doctrine_mapping_relations.png


Pour fonctionner, le mapping des objets doit être configuré. Nous allons décrire cette étape avant de présenter quelques exemples d'utilisations.

.. seealso::

   Les bases de données et Doctrine (documentation symfony)
      http://symfony.com/doc/current/doctrine.html

   Travailler avec les associations doctrine dans Symfony
      https://symfony.com/doc/current/doctrine/associations.html

      La documentation de symfony est la base de cette page.


Configurer le mapping des objets PHP
====================================

Il y a différentes manières de configurer le mapping des objets PHP :

- soit via des annotations, directement dans les classes PHP (C'est la méthode que nous allons utiliser ici) ;
- soit via des documents XML ;
- ou via des fichiers Yaml ;

Voici comment on décrit une classe ``Product`` à l'aide d'annotations :

.. code-block:: php

   // src/AppBundle/Entity/Product.php
   namespace AppBundle\Entity;

   use Doctrine\ORM\Mapping as ORM;

   /**
    * @ORM\Entity
    * @ORM\Table(name="product")
    */
   class Product
   {
       /**
        * @ORM\Column(type="integer")
        * @ORM\Id
        * @ORM\GeneratedValue(strategy="AUTO")
        */
       private $id;

       /**
        * @ORM\Column(type="string", length=100)
        */
       private $name;

       /**
        * @ORM\Column(type="decimal", scale=2)
        */
       private $price;

       /**
        * @ORM\Column(type="text")
        */
       private $description;
   }

.. seealso::

   La configuration du mapping basique dans Doctrine
      http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/basic-mapping.html

      La documentation liste toutes les possibilités de mapping.

.. note::

   Il est possible de générer automatiquement les *getters* et les *setters* de la classe à partir du mapping doctrine à l'aide de la commande suivante :

   .. code-block:: console

      php bin/console doctrine:generate:entities AppBundle/Entity/Product

Générer le schéma automatiquement
=================================

Doctrine peut générer les requêtes SQL qui modifient le schéma de la base de donnée automatiquement :

.. code-block:: console

   php bin/console doctrine:schema:update --force

.. tip::

   Pour gérer l'évolution des schéma de la base de donnée, l'utilisation des commandes doctrine risque de ne pas suffir. Il est conseillé d'utiliser des modules supplémentaires comme `Doctrine Migration <http://www.doctrine-project.org/projects/migrations.html>`_. Doctrine Migration est disponible pour symfony avec `DoctrineMigrationsBundle <http://symfony.com/doc/current/bundles/DoctrineMigrationsBundle/index.html>`_.

   Ces bundles écrivent les requêtes de gestion du schéma dans des fichiers de mirations, qui peuvent être préparés en développement et rejoués en production.

Manipuler les objets
====================

Doctrine propose une api pour manipuler les objets. 

Au centre de cette api se trouve l'objet ``EntityManager`` (`signature <http://www.doctrine-project.org/api/orm/2.5/class-Doctrine.ORM.EntityManagerInterface.html>`_).


Créer une entité
----------------

.. code-block:: php 

   // src/AppBundle/Controller/DefaultController.php

   // ...
   use AppBundle\Entity\Product;
   use Symfony\Component\HttpFoundation\Response;
   use Doctrine\ORM\EntityManagerInterface;
   use Doctrine\Common\Persistence\ManagerRegistry;
   
   class DefaultController extends 
   {
      public function createAction(EntityManagerInterface $em)
      {
          // récupère l'objet "EntityManager"
          $em = $this->getDoctrine()->getManager();

          $product = new Product();
          $product->setName('Keyboard');
          $product->setPrice(19.99);
          $product->setDescription('Ergonomic and stylish!');

          // indique à Doctrine que l'objet peut être persisté (n'est pas en-
          // core enregistré dans la base de donnée
          $em->persist($product);

          // enregistre l'objet dans la BDD (exécute les requêtes INSERT
          $em->flush();

          return new Response('Saved new product with id '.$product->getId());
      }

   }

Récupérer une entité depuis la base de donnée
---------------------------------------------

Il y a plusieurs méthodes pour récupérer les objets :

- soit au moyen de requêtes, via un langage propre à Doctrine: DQL ;
- soit au moyen des *Repository* (`signature <http://www.doctrine-project.org/api/orm/2.5/class-Doctrine.ORM.EntityRepository.html>`_)

Travailler avec les repository
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

L'entity manager peut vous fournir les repository :

.. code-block:: php

   // soit au moyen du nom simplifié :
   $repository = $em->getRepository('AppBundle:Product');

   // soit avec le nom complet :
   $repository = $em->getRepository('AppBundle\Entity\Product');
   // qui peut aussi s'écrire comme suit :
   
   use AppBundle\Entity\Product;
   $repository = $em->getRepository(Product::class);


Le repository possède plusieurs fonctions pratiques pour récupérer un lot d'objet :

.. code-block:: php

   // récupérer par id 
   $product = $repository->find(10);

   // on peut également chercher par la fonction findBy* et findOneBy*

   // ici, on récupère les produits qui coutent 19.99, `price` étant le nom d'une propriété
   $products = $repository->findByPrice(19.99);
   // mais aussi... (en assumant que `$category` est une entité qui existe dans la base de donnée
   $products = $repository->findByCategory($category);
   $product  = $repository->findOneByName('Miel de lavande');

   // récupérer tous les produits
   $products = $repository->findAll();

   // récupérer avec des critères
   $products = $repository->findBy(array(
      'price' => 9.99,
      'category'  => $category
   ));

.. seealso::
   
   Les différentes manières de récupérer les objets de la DBB (documentation doctrine)
      http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/working-with-objects.html#querying


Les requetes DQL
^^^^^^^^^^^^^^^^

DQL, pour **Doctrine Query Language**, permet de construire des requêtes complexes sur les objets :

.. code-block:: php

   $products = $em->createQuery(
      'SELECT p 
      FROM AppBundle:Product p
      WHERE p.price > :price
      ')
      // on ajoute le paramètre 'price'
      ->setParameter('price', 5)
      ->getResult();


.. seealso::

   Documentation du langage DQL (documentation doctrine)
      http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/dql-doctrine-query-language.html

.. tip::
   
   Les requêtes doctrines protègent contre `l'injection SQL <https://fr.wikipedia.org/wiki/Injection_SQL>`_. Il n'est donc pas nécessaire de *nettoyer* les paramètres qui sont passés aux requêtes doctrine.

.. note::

   Doctrine propose une dernière manière de construire des requêtes, le query builder. Cette méthode, très puissante également, permet de créer des requêtes DQL de manière programmatique. 

   Exemple :

   .. code-block:: php

      $query = $repository->createQueryBuilder('p')
          ->where('p.price > :price')
          ->setParameter('price', '19.99')
          ->orderBy('p.price', 'ASC')
          ->getQuery();

      $products = $query->getResult();

Modifier un objet
=================

La modification d'un objet est également très intuitive :

.. code-block:: php

   $product = $em->getRepository('AppBundle:Product')->find($productId);

   if (!$product) {
       throw $this->createNotFoundException(
           'No product found for id '.$productId
       );
   }

   $product->setName('New product name!');
   // la requête UPDATE est effectuée grace à la requête flush
   $em->flush();


