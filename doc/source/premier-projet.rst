
Démarrer mon premier projet
***************************

Installer Symfony
=================

Nous allons d'abord installer l'application de démonstration de symfony, histoire d'inspecter quelques éléments.

1. Créez un répertoire de travail (voir note) ;
2. Téléchargez et installer l'installeur symfony ;
3. Installez l'application "demo" dans le sous-répertoire "demo"

Créer un répertoire de travail
------------------------------

Créer un répertoire ``UDOS`` à un endroit qui vous convient.

.. tip:: 
   Votre répertoire ne doit donc pas se trouver dans un répertoire accessible par apache.

   Depuis la version 5.6, PHP dispose d'un *built-in server*. Il n'est donc pas nécessaire d'installer apache, ou un autre serveur web : rendez-vous dans le répertoire et lancer la commande ``php -S``.

   Symfony dispose également de sa propre commande pour lancer ce serveur en configurant automatiquement les chemins d'accès: ``php bin/console server:run``




Synopsis: Un tour à l'étable 
-----------------------------

Pour ce premier projet, nous devons créer un système de gestion d'étable. 

Notre étable est peuplée de vaches. L'éleveur veut pouvoir suivre son troupeau: encoder les naissances et les arrivées (notre éleveur est végétarien et les vaches ne partiront pas à l'abattoir).

Tous les jours, les vaches sont traites et notre éleveur veut pouvoir encoder la productionde lait. Si elle est trop basse, cela veut dire que notre vache est un peu déprimée et il va falloir lui prendre un rendez-vous chez le vétérinaire: il voudrait être notifié par email qu'un rendez-vous est nécessaire.


Téléchargez l'installer symfony
-------------------------------

Rendez-vous sur https://symfony.com/installer et enregistrez le fichier ``symfony.phar`` dans le répertoire de travail (normalement, ``UDOS``).

.. tip::
   Vous venez de télécharger un fichier `phar`. C'est un format de fichier propre à symfony pour distribuer une application complète. `En savoir plus <http://php.net/manual/fr/phar.using.intro.php>`_.

   Par convention, vous pouvez ajouter l'argument `help` pour obtenir de l'aide, ou `list` pour imprimer la liste des commandes : `php symfony.phar help` `php symfony.phar list`.

   Sous linux, vous pouvez également supprimer le `php` au début des commandes en rendant le fichier exécutable: `chmod +x symfony.phar`. Si vous souhaitez que la commande soit accessible pour tout le système, enregistrez-là sous ``/usr/local/bin/symfony``. Vous aurez alors accès à la commande en utilisant tout simplement ``symfony``.

.. note::
   **Une application _demo_ pour symfony**

   Symfony propose une application de démonstration. Pour l'installer, lancez simplement la commande ``php symfony.phar demo <nom du répertoire>``

   Cela va installer l'application de démonstration de symfony, et l'enregistrer dans le répertoire indiqué.


Créer notre application "etable"
--------------------------------

Lancez la commande ``php symfony.phar new etable``. Cela va créer une nouvelle application (l'argument ``ǹew``) dans le répertoire ``etable``. Ceci devrait s'afficher ::

  Downloading Symfony...

    5.8 MiB/5.8 MiB ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓  100%

  Preparing project...

  ✔  Symfony 3.3.2 was successfully installed. Now you can:

    * Change your current directory to /home/julien/dev/formation-symfony/UDOS/src/etable

    * Configure your application in app/config/parameters.yml file.

    * Run your application:
        1. Execute the php bin/console server:start command.
        2. Browse to the http://localhost:8000 URL.

    * Read the documentation at http://symfony.com/doc


Vous pouvez donc vous déplacer dans le répertoire ``ètable`` et lancer la commande ``php bin/console server:run``. Le *built-in server* de PHP va être lancé avec la configuration ad hoc, et l'application lancée. En allant sur http://localhost:8000 vous devriez voir apparaitre cette page : 

.. figure:: _static/homepage_symfony_first_app.png
   
   La page qui s'affiche à la première installation de symfony. 

.. tip::
   La sortie de la commande vous propose d'utiliser ``php bin/console server:start`` pour afficher. Cela est également possible, mais lancera le serveur en tâche de fond. L'avantage de pouvoir garder le processus en console est que, si votre serveur php "tombe" en cas de bug, vous serez directement informés. Vous pourrez aussi consulter les logs du serveur.

.. note:: 
   La barre de développement apparait dans le bas des pages. Elle va être votre compagnon pour déboguer votre application. Elle n'est affichée qu'en version de développement. N'hésitez pas à cliquer sur les différents liens pour voir ce qui s'y cache.

   **Elle n'apparait pas ?** Vérifiez que votre code HTML est valide. En effet, il s'agit d'une balise html qui insérée avant la balise `</body>`. Si votre page html n'est pas valide, la barre de développement n'est pas affichée.

   .. figure:: _static/barre_de_developpement_symfony.png

   La barre de développement de symfony.

Découvrir l'arborescence de Symfony
===================================

La commande ``symfony new`` a créé une arborescence de différentes répertoires [#]_::

   ├── app
   │   ├── AppCache.php
   │   ├── AppKernel.php
   │   ├── config
   │   │   ├── config_dev.yml
   │   │   ├── config_prod.yml
   │   │   ├── config_test.yml
   │   │   ├── config.yml
   │   │   ├── parameters.yml
   │   │   ├── parameters.yml.dist
   │   │   ├── routing_dev.yml
   │   │   ├── routing.yml
   │   │   ├── security.yml
   │   │   └── services.yml
   │   └── Resources
   │       └── views
   ├── bin
   │   ├── console
   │   └── symfony_requirements
   ├── composer.json
   ├── composer.lock
   ├── phpunit.xml.dist
   ├── README.md
   ├── src
   │   └── AppBundle
   │       ├── AppBundle.php
   │       └── Controller
   ├── tests
   │   └── AppBundle
   │       └── Controller
   ├── var
   │   ├── bootstrap.php.cache
   │   ├── cache
   │   │   └── dev
   │   ├── logs
   │   │   └── dev.log
   │   ├── sessions
   │   │   └── dev
   │   └── SymfonyRequirements.php
   ├── vendor
   │   ├── autoload.php
   │   ├── bin
   │   │   ├── doctrine -> ../doctrine/orm/bin/doctrine
   │   │   ├── doctrine-dbal -> ../doctrine/dbal/bin/doctrine-dbal
   │   │   ├── doctrine.php -> ../doctrine/orm/bin/doctrine.php
   │   │   ├── security-checker -> ../sensiolabs/security-checker/security-checker
   │   │   └── simple-phpunit -> ../symfony/phpunit-bridge/bin/simple-phpunit
   │   ├── composer
   │   │   ├── autoload_classmap.php
   │   │   ├── autoload_files.php
   │   │   ├── autoload_namespaces.php
   │   │   ├── autoload_psr4.php
   │   │   ├── autoload_real.php
   │   │   ├── autoload_static.php
   │   │   ├── ca-bundle
   │   │   ├── ClassLoader.php
   │   │   ├── installed.json
   │   │   └── LICENSE
   │   ├── doctrine
   │   │   ├── annotations
   │   │   ├── cache
   │   │   ├── collections
   │   │   ├── common
   │   │   ├── dbal
   │   │   ├── doctrine-bundle
   │   │   ├── doctrine-cache-bundle
   │   │   ├── inflector
   │   │   ├── instantiator
   │   │   ├── lexer
   │   │   └── orm
   │   ├── fig
   │   │   └── link-util
   │   ├── incenteev
   │   │   └── composer-parameter-handler
   │   ├── jdorn
   │   │   └── sql-formatter
   ├── symfony
   │   │   ├── monolog-bundle
   │   │   ├── phpunit-bridge
   │   │   ├── polyfill-apcu
   │   │   ├── polyfill-intl-icu
   │   │   ├── polyfill-mbstring
   │   │   ├── polyfill-php56
   │   │   ├── polyfill-php70
   │   │   ├── polyfill-util
   │   │   ├── swiftmailer-bundle
   │   │   └── symfony
   │   └── twig
   │       └── twig
   └── web
       ├── app_dev.php
       ├── apple-touch-icon.png
       ├── app.php
       ├── bundles
       ├── config.php
       ├── favicon.ico
       └── robots.txt

Les répertoires à remarquer sont les suivants :

``app``
   Contient les éléments comme de la configuration et des templates spécifiques. Il y a très peu de PHP dans ce répertoire.

   La configuration est définie dans différents environnements. Symfony crée trois environements par defaut. Voir note ci-dessous.

``src``
   C'est ici que vous ajouterez votre code PHP.

``bin``
   Les fichiers exécutables sont indiqués ici. C'est là que la console est enregistrée.

``var``
   Les fichiers créés automatiquement, comm les logs, le cache et les sessions, sont enregistrés ici.

``vendor``
   Ici se trouvent les fichiers de librairies et d'outils tiers. 

``web``
   Le répertoire public de symfony. C'est là que devront se trouver (ou se recopier) les fichiers d'asset (javascript, css, etc.).

``tests``
   C'est ici que vous écrirez des tests automatiques (tests unitaires et tests fonctionnels) pour votre application.

.. note:: 

   **Les environnements dans Symfony**

   Symfony définit trois environnements :

   - ``dev`` pour le développement. C'est celui là qui est utilisé quand on exécute ``php bin/console server:run``. Dans cet environnement, plusieurs outils de débogage (la barre de développement, etc.) sont activés.
   - ``test`` pour faire tourner les tests unitaires. 
   - ``prod`` pour la production.

   Ces environnements sont une configuration différentes. Par exemple, le niveau de rétention des logs va être différent, selon que l'on est en environnement de production ou de test. Certains éléments de configuration sont communs, ils sont dans le fichier ``app/config/config.yml``.


Créer notre bundle
==================

Dans symfony, le code est réparti dans des *Bundles*. A priori, chaque bundle doit exercer un certain "métier" et avoir une certaine cohérence. Par exemple, si notre application gérerait des vaches et, plus tard, de la culture maraichère, nous voudrions sûrement séparer notre code en deux bundles: l'un pour les vaches, l'autres pour la culture de plantation. 

Il existe également des bundles _tiers_, qui ont été téléchargés automatiquement pour vous. Ils sont présents dans le répertoire ``vendor``. Vous devriez y trouver Monolog (gestion des logs), Doctrine (ORM), et... le framework symfony, qui est lui-même un assemblage de différents bundles.

Au démarrage, tous les bundles sont chargés dans le fichier ``AppKernel.php``, par la méthode ``registerBundles`` :

.. code-block:: php

	use Symfony\Component\HttpKernel\Kernel;
	use Symfony\Component\Config\Loader\LoaderInterface;

	class AppKernel extends Kernel
	{
	    public function registerBundles()
	    {
		// un tableau avec tous les bundles chargés
		$bundles = [
		    new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
		    new Symfony\Bundle\SecurityBundle\SecurityBundle(),
		    new Symfony\Bundle\TwigBundle\TwigBundle(),
		    new Symfony\Bundle\MonologBundle\MonologBundle(),
		    new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
		    new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
		    new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
		    new AppBundle\AppBundle(),
		    new EtableBundle\EtableBundle(),
		];

		// des bundles spécifiques aux environnements de developpement et de test
		if (in_array($this->getEnvironment(), ['dev', 'test'], true)) {
		    $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
		    $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
		    $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();

		    if ('dev' === $this->getEnvironment()) {
			$bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
			$bundles[] = new Symfony\Bundle\WebServerBundle\WebServerBundle();
		    }
		}

		return $bundles;
	    }

	    // ...
	}

Par défaut, l'installeur de Symfony a créé l'``AppBundle``. Par souci de cohérence, nous allons créer notre propre Bundle, que nous allons nommer ``EtableBundle``.

Il existe une commande pour celà! Lancez donc la commande ``php bin/console generate:bundle`` et suivez les instructions::

					       
   Welcome to the Symfony bundle generator!  
					       

   Are you planning on sharing this bundle across multiple applications? [no]: no

   Your application code must be written in bundles. This command helps
   you generate them easily.

   Give your bundle a descriptive name, like BlogBundle.
   Bundle name: EtableBundle

   Bundles are usually generated into the src/ directory. Unless you're
   doing something custom, hit enter to keep this default!

   Target Directory [src/]: 

   What format do you want to use for your generated configuration?

   Configuration format (annotation, yml, xml, php) [annotation]: 

			
     Bundle generation  
			

   > Generating a sample bundle skeleton into app/../src/EtableBundle
     created ./app/../src/EtableBundle/
     created ./app/../src/EtableBundle/Etable2Bundle.php
     created ./app/../src/EtableBundle/Controller/
     created ./app/../src/EtableBundle/Controller/DefaultController.php
     created ./app/../tests/EtableBundle/Controller/
     created ./app/../tests/EtableBundle/Controller/DefaultControllerTest.php
     created ./app/../src/EtableBundle/Resources/views/Default/
     created ./app/../src/EtableBundle/Resources/views/Default/index.html.twig
     created ./app/../src/EtableBundle/Resources/config/
     created ./app/../src/EtableBundle/Resources/config/services.yml
     created ./app/../src/Etable2Bundle/Resources/config/routing.yml
   > Checking that the bundle is autoloaded
   > Enabling the bundle inside app/AppKernel.php
     updated ./app/AppKernel.php
   > Importing the bundle's routes from the app/config/routing.yml file
     updated ./app/config/routing.yml
   > Importing the bundle's services.yml from the app/config/config.yml file
     updated ./app/config/config.yml

					    
     Everything is OK! Now get to work :).  
					    
Vous remarquerez que les fichiers de configuration ont été modifiés :

- La console a ajouté le bundle dans ``AppKernel``
- les routes (voir plus loin) sont ajoutées dans le fichier ``app/config/routing.yml``
- la configuration des services est importées dans le fichier de configuration général, ``app/config/config.yml``


Notre première page
===================

Nous allons maintenant créer notre première page: un "tableau" pour que nos éleveurs puissent avoir toute l'info utile sous les yeux. Ce "dashboard" sera accessible sous l'url ``/dashboard`` (et, donc http://localhost:8000/dashboard). Pour l'instant, ce dashboard sera vide.

- Nous allons créer le controleur. A l'intérieur, nous allons créer une action "dashboard";
- puis modifier le template créé automatiquement par symfony ;

Créer le controleur
-------------------

Il existe une commande pour cela: ``php bin/console generate:controller``. Le controleur aura le nom ``EtableBundle:Dashboard``, le format de route ``annotation`` et une seule page action sera créée: ``dashboardAction`` Les autres paramètres par défaut peuvent être conservés::

						       
	  Welcome to the Symfony controller generator  
						       


	Every page, and even sections of a page, are rendered by a controller.
	This command helps you generate them easily.

	First, you need to give the controller name you want to generate.
	You must use the shortcut notation like AcmeBlogBundle:Post

	Controller name: EtableBundle:Dashboard

	Determine the format to use for the routing.

	Routing format (php, xml, yml, annotation) [annotation]: 

	Determine the format to use for templating.

	Template format (twig, php) [twig]: 

	Instead of starting with a blank controller, you can add some actions now. An action
	is a PHP function or method that executes, for example, when a given route is matched.
	Actions should be suffixed by Action.


	New action name (press <return> to stop adding actions): dashboardAction
	Action route [/dashboard]: 
	Template name (optional) [EtableBundle:Dashboard:dashboard.html.twig]: 

	New action name (press <return> to stop adding actions): 

				     
	  Summary before generation  
				     

	You are going to generate a "EtableBundle:Dashboard" controller
	using the "annotation" format for the routing and the "twig" format
	for templating
	Do you confirm generation [yes]? yes

				 
	  Controller generation  
				 

	  created ./src/EtableBundle/Resources/views/Dashboard/
	  created ./src/EtableBundle/Resources/views/Dashboard/dashboard.html.twig
	  created ./src/EtableBundle/Controller/DashboardController.php
	  created ./src/EtableBundle/Tests/Controller/
	  created ./src/EtableBundle/Tests/Controller/DashboardControllerTest.php
	Generating the bundle code: OK

						 
	  Everything is OK! Now get to work :).  
						 


Maintenant l'URL devrait afficher le message suivant :

.. figure:: _static/welcome_to_dashboard_page.png

   La page par défaut

Inspectons le code qui a été ajouté. Un premier controleur avec une action dashboard a été créé :

- un controleur, avec le code suivant:

.. code-block:: php

   namespace EtableBundle\Controller;

   use Symfony\Bundle\FrameworkBundle\Controller\Controller;
   use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

   class DashboardController extends Controller
   {
       /**
        * @Route("/dashboard")
        */
       public function dashboardAction()
       {
           return $this->render('EtableBundle:Dashboard:dashboard.html.twig', array(
               // ...
           ));
       }

   }

- un fichier de vue, avec le code suivant :

.. code-block:: html+jinja

   {% extends "::base.html.twig" %}

   {% block title %}EtableBundle:Dashboard:dashboard{% endblock %}

   {% block body %}
   <h1>Welcome to the Dashboard:dashboard page</h1>
   {% endblock %}

Le controleur appelle la vue grâce à la fonction ``render``. 


.. note::

   A ce stade, vous voudrez peut-être utiliser un IDE, comme Netbeans. Installez netbeans PHP et créer un projet "PHP > PHP application with existing sources". Pointez le répertoire vers votre répertoire "etable", dans UDOS.

   Vous bénéficierez alors de fonctionnalités avancées, comme l'auto-complétion.

Modifier le template
--------------------

Le template est écrit en twig, mais il contient du HTML. Remplacez le titre entre les balises ``h1`` par un message d'accueil sur le dashboard.

Nous allons également modifier le squelette du template. Comme vous le remarquerez, le template hérite du template ``base.html.twig``, via la balise ``{% extends "::base.html.twig" %}```. Allez dans ce fichier et ajoutez bootstrap depuis un CDN en ajoutant ce code entre les balises ``head`` du template :

.. code-block:: html

   <!-- Latest compiled and minified CSS -->
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

   <!-- Optional theme -->
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

Ajoutez également le javascript nécessaire pour les plugins de bootstrap. Placez-les à la fin du ``body``, mais avant le block ``javascript``.

.. code-block:: html

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
   <!-- Latest compiled and minified JavaScript -->
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


Vous allez également ajouter une barre de navigation, dont voici le code qui est à placer au début de la balise ``body``:

.. code-block:: html

   <nav class="navbar navbar-default">
     <div class="container-fluid">
       <!-- Brand and toggle get grouped for better mobile display -->
       <div class="navbar-header">
         <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
           <span class="sr-only">Toggle navigation</span>
           <span class="icon-bar"></span>
           <span class="icon-bar"></span>
           <span class="icon-bar"></span>
         </button>
         <a class="navbar-brand" href="#">Mon étable</a>
       </div>

       <!-- Collect the nav links, forms, and other content for toggling -->
       <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
         <ul class="nav navbar-nav">
           <li class="active"><a href="#">Une action plus tard <span class="sr-only">(current)</span></a></li>
           <li><a href="#">Une autre action plus tard</a></li>
         </ul>
         
       </div><!-- /.navbar-collapse -->
     </div><!-- /.container-fluid -->
   </nav>

.. seealso:: 
   Notions de "template" et d'héritage en twig :
      http://symfony.com/doc/current/templating.html#template-inheritance-and-layouts
  
   Documentation de twig sur le mot-clé ``extends``
       https://twig.sensiolabs.org/doc/2.x/tags/extends.html

Et la vache fut
===============

Maintenant que nous avons découvert les controleurs et les vues, nous allons nous attaquer à la partie "Modèle" de notre application. Nous allons commencer par définir ce qui est la base d'un élevage: une vache.

Chacune de nos vaches va avoir les caractéristiques suivantes : 

``id``
   un identifiant unique, qui sera sous la forme d'un entier. L'outil Doctrine va générer lui-même cet entier.

``name``
   un nom: une chaine de caractère ;

``race``
   une race de vache. Pour plus de simplicité, il s'agira d'une chaine de caractères également ;

``birthdate``
   une date de naissance. La donnée sera stockée au format date (``Datetime`` en PHP) ;

``afscaid``
   un identifiant pour les autorités alimentaires. `Il s'agit d'un code qui commence par deux lettres, suivi de 9 chiffres <http://www.afsca.be/productionanimale/animaux/identification/_documents/modeleUtilise1999-11-01.pdf>`_

``color``
   la couleur de sa robe ;

Nous ajouterons également cette autre information :

``datecreation``
   la date à laquelle la vache a été encodée dans le système pour la première fois ;

Plus tard, nous ajouterons d'autres éléments.

Notre vache va correspondre à une classe ``Cow``, avec le namespace complet ``EtableBundle\Entity\Cow`` Elle sera donc dans le répertoire ``Entity``. Notez que Symfony permettra de raccourcir son nom selon la syntaxe suivante: ``EtableBundle:Cow``, c'est ce que nous utiliserons plus tard.

Il existe une commande pour créer cet élément; cependant il faut que la base de donnée soit configurée pour pouvoir créer notre classe. Il faut donc configurer la base de donnée.

Configurer la base de donnée SQLITE
-----------------------------------

Nous allons donc plonger pour la première fois dans la configuration de notre application, dans le fichier ``àpp/config/config.yml``. 

.. code-block:: yaml

   # Doctrine Configuration
   doctrine:
       dbal:
           driver: pdo_pgsql
           # toutes les valeurs entre '%' viennent de variables extérieures. Ici, elles sont définies dans le fichier parameters.yml.
           host: '%database_host%'
           port: '%database_port%'
           dbname: '%database_name%'
           user: '%database_user%'
           password: '%database_password%'
           charset: UTF8
           # if using pdo_sqlite as your database driver:
           #   1. add the path in parameters.yml
           #     e.g. database_path: "%kernel.project_dir%/var/data/data.sqlite"
           #   2. Uncomment database_path in parameters.yml.dist
           #   3. Uncomment next line:
           #path: '%database_path%'


.. tip::
  
   Pourquoi stocker des variables dans ``parameters.yml`` ?

   Ce fichier contient généralement des variables secrètes: le mot de passe des bases de données, etc. Il peut aussi contenir des informations spécifiques à une installation.

   Ce fichier n'est **pas commité**. Il n'est donc pas partagé publiquement sur internet, et ces variables peuvent rester secrètes.

Nous allons donc modifier le fichier ``app/config/parameters.yml`` pour configurer sqlite :

.. code-block:: yaml

   parameters:
       #ces éléments ne sont pas nécessaires pour sqlite :
       #database_host: 127.0.0.1
       #database_port: null
       #database_name: symfony
       #database_user: root
       #database_password: null
       
       # *** Définition de SQlite ***
       # ici, on veut créer la base de donnée dans un répertoire personnel
       # n'oubliez pas de créer ce répertoire
       database_path: '/home/julien/udos/db.sqlite'

       mailer_transport: smtp
       mailer_host: 127.0.0.1
       mailer_user: null
       mailer_password: null
       secret: 1707a2bb0efe63d283d37625c77f3cb1f4ad4188

Et le fichier ``config.yml``

.. code-block:: yaml

   # Doctrine Configuration
   doctrine:
       dbal:
           driver: pdo_sqlite
           charset: UTF8
           path: '%database_path%'

.. tip::

   Si vous utilisez sqlite **en production**, notez que le répertoire parent de la base de donnée doit être accessible en modification par l'utilisateur qui exécute le script php. En effet, lorsqu'il manipule les données (requêtes ``INSERT`` par exemple), Sqlite créer un ficheir temporaire qui indique qu'il va faire des modifications, et que d'autres processus doivent attendre avant de modifier le fichier. 

   En production, la plupart du temps, l'utilisateur ``ẁww-data`` (gid: 33) doit donc avoir des droits d'écriture **sur le répertoire parent**.  C'est une erreur commune et votre serviteur a été piégé plusieurs fois...

   Mais en test, en utilisant la commande ``server:run``, il n'y a pas de problème: l'utilisateur qui exécute le serveur est... vous !

Créer notre entité ``Cow``
--------------------------

Nous pouvons maintenant lancer la commande ``php bin/console doctrine:generate:entity`` et répondre aux questions suivantes :

.. code-block:: bash

   This command helps you generate Doctrine2 entities.

   First, you need to give the entity name you want to generate.
   You must use the shortcut notation like AcmeBlogBundle:Post.

   The Entity shortcut name: EtableBundle:Cow

   Determine the format to use for the mapping information.

   Configuration format (yml, xml, php, or annotation) [annotation]: 

   Instead of starting with a blank entity, you can add some fields now.
   Note that the primary key will be added automatically (named id).

   Available types: array, simple_array, json_array, object, 
   boolean, integer, smallint, bigint, string, text, datetime, datetimetz, 
   date, time, decimal, float, binary, blob, guid.

   New field name (press <return> to stop adding fields): name
   Field type [string]: 
   Field length [255]: 
   Is nullable [false]: 
   Unique [false]: true

   New field name (press <return> to stop adding fields): race
   Field type [string]: 
   Field length [255]: 50
   Is nullable [false]: true
   Unique [false]: false

   New field name (press <return> to stop adding fields): birthdate
   Field type [string]: date
   Is nullable [false]: false
   Unique [false]: 

   New field name (press <return> to stop adding fields): afscaid
   Field type [string]: 
   Field length [255]: 11
   Is nullable [false]: 
   Unique [false]: true

   New field name (press <return> to stop adding fields): color
   Field type [string]: 
   Field length [255]: 25
   Is nullable [false]: true
   Unique [false]: 

   New field name (press <return> to stop adding fields): datecreation
   Field type [string]: datetime
   Is nullable [false]: 
   Unique [false]: 

   New field name (press <return> to stop adding fields): 

                        
     Entity generation  
                        

     created ./src/EtableBundle/Entity/
     created ./src/EtableBundle/Entity/Cow.php
   > Generating entity class src/EtableBundle/Entity/Cow.php: OK!
   > Generating repository class src/EtableBundle/Repository/CowRepository.php: OK!

                                            
     Everything is OK! Now get to work :).  
                                               

Un premier fichier qui décrit notre vache a été créé: 

.. literalinclude:: _static/code/Cow.php.1
   :language: php
   :linenos:

.. seealso::

   La documentation sur le mapping des objets doctrine
      http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/basic-mapping.html

Générer le schéma
-----------------

C'est le moment de générer le schéma de la base de donnée. Une commande existe pour cela::

   php bin/console doctrine:schema:create

L'option ``--dump-sql`` permet de voir le SQL qui va être généré::

   $ php bin/console doctrine:schema:create --dump-sql
   CREATE TABLE cow (id INTEGER NOT NULL, name VARCHAR(255) NOT NULL, race VARCHAR(50) DEFAULT NULL, birthdate DATE NOT NULL, afscaid VARCHAR(11) NOT NULL, color VARCHAR(25) DEFAULT NULL, datecreation DATETIME NOT NULL, PRIMARY KEY(id));
   CREATE UNIQUE INDEX UNIQ_99D43F9C5E237E06 ON cow (name);
   CREATE UNIQUE INDEX UNIQ_99D43F9C4E146222 ON cow (afscaid);

Après la création, vous pouvez inspecter la base de donnée avec sqliteman [#]_. 

.. figure:: _static/cow1_dans_sqliteman.png

   La table 'cow' dans Sqliteman.


CRUD la vache
=============

Nous allons maintenant ajouter le code qui nous permettra de créer les requêtes CRUD (CReate, Update, Delete) de la vache via des pages web.

Encore une fois, une commande existe pour cela::

   php bin/console generate:doctrine:crud

On peut répondre à quelques questions...::

   This command helps you generate CRUD controllers and templates.

   First, give the name of the existing entity for which you want to generate a CRUD
   (use the shortcut notation like AcmeBlogBundle:Post)

   The Entity shortcut name: EtableBundle:Cow

   By default, the generator creates two actions: list and show.
   You can also ask it to generate "write" actions: new, update, and delete.

   Do you want to generate the "write" actions [no]? yes

   Determine the format to use for the generated CRUD.

   Configuration format (yml, xml, php, or annotation) [annotation]: 

   Determine the routes prefix (all the routes will be "mounted" under this
   prefix: /prefix/, /prefix/new, ...).

   Routes prefix [/cow]: 

                             
   Summary before generation  
                             

   You are going to generate a CRUD controller for "EtableBundle:Cow"
   using the "annotation" format.

   Do you confirm generation [yes]? 

                   
   CRUD generation  
                   

   created ./src/EtableBundle/Controller//CowController.php
   created ./app/Resources/views/cow/
   created ./app/Resources/views/cow/index.html.twig
   created ./app/Resources/views/cow/show.html.twig
   created ./app/Resources/views/cow/new.html.twig
   created ./app/Resources/views/cow/edit.html.twig
   created ./src/EtableBundle/Tests/Controller//CowControllerTest.php
   Generating the CRUD code: OK
   created ./src/EtableBundle/Form/
   created ./src/EtableBundle/Form/CowType.php
   Generating the Form code: OK
   Updating the routing: OK

                                         
   Everything is OK! Now get to work :).  
                                         

Et le code par défaut permet d'avoir une nouvelle route: http://localhost:8000/cow

La commande a donc créé: 

- un nouveau controleur: ``Controller//CowController.php``
- les templates adéquats dans ``/app/Resources/views/cow/``
- un test automatique ;
- un formulaire pour ajouter et modifier les vaches : ``/src/EtableBundle/Form/CowType.php``
- et mis à jour les routes

Cependant, les pages ne sont pas très agréables à regarder :

.. figure:: _static/cow1_initial_liste.png

   La liste des vaches (vide pour le moment).

.. figure:: _static/cow1_initial_form.png

   Le formulaire pour ajouter une vache.

Nous allons donc améliorer tout ça...

Des beaux formulaires
---------------------

Symfony fournit des templates par défaut pour les formulaires.

Nous allons utiliser le template *horizontal bootsrap*. Il suffit d'ajouter deux lignes dans la configuration :

.. code-block:: yaml

   # Twig Configuration
   twig:
       debug: '%kernel.debug%'
       strict_variables: '%kernel.debug%'
       # Ces deux lignes vont permettre d'utiliser le template twig pour bootstrap
       form_themes:
           - bootstrap_3_horizontal_layout.html.twig #le template twig pour bootstrap

.. figure:: _static/cow1_initial_form_bootstrap.png

   Avec le template bootstrap, le formulaire est beaucoup plus agréable.


.. seealso::

   La référence de la configuration twig dans symfony
      http://symfony.com/doc/current/reference/configuration/twig.html

   Comprendre comment customizer le rendu des formulaires
      http://symfony.com/doc/current/form/form_customization.html


Des formulaires selon nos choix
-------------------------------

Les formulaires créés automatiquement ne nous conviennent cependant pas tout à fait :

- les ``label`` des formulaires ne sont pas très explicites. Nous allons les corriger ;
- Nous voudrions créer une liste à choix multiple des races de vaches, plutôt que d'avoir un champ texte ;
- Nous voudrions également une liste à choix multiple des couleurs de vache ;
- enfin, la date de création ne doit pas être ajoutée par le formulaire, mais automatiquement à la création de la vache.

Nous allons donc intervenir sur la classe ``EtableBundle\Form\CowType`` pour modifier la composition du formulaire. Nous allons travailler sur la fonction ``buildForm`` qui est, pour l'instant, rédigé comme suit :

.. code-block:: php

   namespace EtableBundle\Form;

   use Symfony\Component\Form\AbstractType;
   use Symfony\Component\Form\FormBuilderInterface;
   use Symfony\Component\OptionsResolver\OptionsResolver;

   class CowType extends AbstractType
   {
       /**
        * {@inheritdoc}
        */
       public function buildForm(FormBuilderInterface $builder, array $options)
       {
           $builder->add('name')->add('race')->add('birthdate')->add('afscaid')->add('color')->add('datecreation');
       }

       // d'autres fonctions ici
   }

Nous modifions d'abord la class ``EtableBundle\Entity\Cow`` pour y ajouter : 

- la génération automatique de la date de création ;
- des constantes pour stocker les valeurs des listes de choix (la couleur et la race)

.. code-block:: php

   namespace EtableBundle\Entity;

   use Doctrine\ORM\Mapping as ORM;

   /**
    * Cow
    *
    * @ORM\Table(name="cow")
    * @ORM\Entity(repositoryClass="EtableBundle\Repository\CowRepository")
    */
   class Cow
   {
       // les variables restent inchangées 
    
       const RACE_BBB       = 'bbb';
       const RACE_HOLSTEIN  = 'holstein';
       const RACE_ABONDANCE = 'abondance';
       const RACE_LIMOUSINE = 'limousine';
       const RACE_NORMANDE  = 'normande';

       const COLOR_WHITE  = 'white';
       const COLOR_BLACK_WHITE = 'black_white';
       const COLOR_BROWN   = 'brown';
       const COLOR_BLACK   = 'black';
       
       public function __construct()
       {
           $this->datecreation = new \DateTime('now');
       }
   }

Puis, nous améliorons le formulaire : 

.. literalinclude:: _static/code/CowType.php.2
   :language: php


.. seealso::

   La références des types de formulaire
      Symfony définit différents types de formulaires, avec chacun leurs propres options. `Vous pouvez retrouver leur liste complète et leurs options dans la documentation  <http://symfony.com/doc/current/reference/forms/types.html>`_

Des formulaires justes
----------------------

Les formulaires sont plus agréables, mais ils permettent encore aux utilisateurs d'introduire des erreurs. Nous allons ajouter des fonctions de validation au formulaire pour éviter la saisie de données erronées : 

- le nom des vaches doit contenir au moins trois caractères ;
- le numéro de la bague doit être composé de deux lettres puis 9 chiffres.

.. seealso::

   La documentation de symfony sur la validation
      `La documentation liste les contraintes existantes dans Symfony, et les arguments qu'ils peuvent recevoir. <http://symfony.com/doc/current/validation.html>`_

Après modification, le code devrait ressembler à ceci :

.. code-block:: php

   <?php

   namespace EtableBundle\Entity;

   use Doctrine\ORM\Mapping as ORM;
   // pour pouvoir utiliser la validation, nous importons la classe nécessaire
   use Symfony\Component\Validator\Constraints as Assert;

   /**
    * Cow
    *
    * @ORM\Table(name="cow")
    * @ORM\Entity(repositoryClass="EtableBundle\Repository\CowRepository")
    */
   class Cow
   {
       // ...

       /**
        * @var string
        *
        * @ORM\Column(name="name", type="string", length=255, unique=true)
        * @Assert\NotNull()
        * @Assert\Length(
        *      min = 3,
        *      minMessage = "Le nom de la vache doit contenir au moins {{ limit }} trois caractères."
        *  )
        */
       private $name;

       // ...

       /**
        * @var string
        *
        * @ORM\Column(name="afscaid", type="string", length=11, unique=true)
        * @Assert\Regex(
        *      pattern = "/[a-zA-Z]{2}[0-9]{9}/",
        *      message = "Le code semble incorrect, désolé..."
        *  )
        */
       private $afscaid;
   
       // ...
   }

Désormais, dès qu'une entrée est invalide, Symfony va la rejeter :

.. figure:: _static/cow1_form_with_validation_false.png

   Un formulaire avec des données incorrectes: des messages d'erreurs s'affichent à l'utilisateur.



Améliorer les vues
------------------

Nous pouvons profiter de notre découverte des vues pour:

- traduire les phrases en français ;
- ajouter des titres aux pages (un block ``title``) ;
- intégrer une classe CSS bootstrap pour mettre en forme le tableau (``class="table table-striped"``) ;


.. note::

   C'est également l'occasion de commenter quelque peu l'articulation entre les controleurs et la vue.

   Examinons le code du controleur de l'action ``show``, qui affiche une seule vache :

   .. code-block:: php

      <?php

      namespace EtableBundle\Controller;

      use EtableBundle\Entity\Cow;
      use Symfony\Bundle\FrameworkBundle\Controller\Controller;
      use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
      use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

      /**
       * Cow controller.
       *
       * @Route("cow")
       */
      class CowController extends Controller
      {
          // ...
          
          /**
           * Finds and displays a cow entity.
           *
           * @Route("/{id}", name="cow_show")
           * @Method("GET")
           */
          public function showAction(Cow $cow)
          {
              $deleteForm = $this->createDeleteForm($cow);

              return $this->render('cow/show.html.twig', array(
                  'cow' => $cow,
                  'delete_form' => $deleteForm->createView(),
              ));
          }

          // ...
      }

   Commentaires : 

   ``@Route("cow")``
   
   La route est préfixée pour tout le controleur

   ``@Route("/{id}", name="cow_show")``
   
   Une nouvelle route est créée, qui contient juste l'id de la vache. Elle est nommée cow_show

   ``public function showAction(Cow $cow)``
   
   Lorsque l'argument est préfixé, l'identifiant Id permet de récupéré l'objet ``Cow`` automatiquement. 

   .. code-block:: php

      return $this->render('cow/show.html.twig', array(
                  'cow' => $cow,
                  'delete_form' => $deleteForm->createView(),
              ));
   
   Ici, le controleur effectue le rendu de la vue (fonction ``render`` en y incluant des paramètres spécifiques, notamment ``cow``. Ce paramètre peut dès lors être utilisé dans la vue twig, dans la variable ``cow``. 

   Twig résout automatiquement les variables :

   .. code-block:: html+jinja

      {{ cow.name }} 
      {# 
         twig va tester:
         - $cow['name'] (un tableau associatif) ;
         - $cow->getName() (un objet avec une fonction getName)
         - $cow->isName(), (un objet avec une fonction isName)
         - etc.
       %}
      

Ajouter des liens dans notre barre de navigation
================================================

Pour pouvoir accéder directement à la liste des vaches, nous allons ajouter un lien dans la barre de navigation, dans le template ``app/Resources/view/base.html.twig``

Pour rappel, cette route est nommée ``cow_index``. On peut retrouver ce nom dans les annotations du controleur.

Nous allons également ajouter un lien vers la racine du site sous le titre "Mon étable". Pour ce faire, nous allons devoir nommer la route dans ``DefaultControlleur``, nous allons l'appeler ``home``.

.. code-block:: html+jinja

   <nav class="navbar navbar-default">
   <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <!-- nous utilisons la fonction 'path' de twig -->
      <a class="navbar-brand" href="{{ path('home') }}">Mon étable</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active">
            <!-- nous ajoutons également ici un lien -->
            <a href="{{ path('cow_index') }}">Nos vaches<span class="sr-only">(current)</span></a>
        </li>
        <li><a href="#">Une autre action plus tard</a></li>



.. seealso::

   Comment générer des routes dans un controleur
      http://symfony.com/doc/current/routing.html#generating-urls

   Comment générer des routes dans un template twig
      http://symfony.com/doc/current/templating.html#linking-to-pages


Plongeons dans les controleurs
==============================

Il est temps, maintenant, de plonger dans les controleurs et d'en composer nous-mêmes.

Les controleurs renvoient des réponses
--------------------------------------

La fonction ``render`` que nous avons vu plus haut renvoie un objet de type ``Response`` `signature <http://api.symfony.com/3.3/Symfony/Component/HttpFoundation/Response.html>`_.


Juste pour découvrir, nous allons modifier la réponse renvoyée par la fonction ``DefaultController::indexAction`` :

.. code-block:: php

   <?php

   namespace EtableBundle\Controller;

   use Symfony\Bundle\FrameworkBundle\Controller\Controller;
   use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
   use Symfony\Component\HttpFoundation\Response;

   class DefaultController extends Controller
   {
       /**
        * @Route("/", name="home")
        */
       public function indexAction()
       {
           return new Response("Aidons les vaches à ruminer, dit le bienveillant, "
               . "posant sa fourchette un instant. ");
       }
   }

La réponse est immédiatement modifiée :

.. figure:: _static/test_controlleur_dummy_response.png

   Notre controleur nous renvoie bien une nouvelle réponse. 

Mais ça n'est pas encore très utile. Nous allons donc maintenant renvoyer une réponse de redirection, qui renvoie vers le dashboard. Nous allons pour cela utiliser la classe ``RedirectResponse`` `(signature) <http://api.symfony.com/3.3/Symfony/Component/HttpFoundation/RedirectResponse.html>`_.

Nous modifions donc le controleur ``DefaultController::indexAction`` :

.. code-block:: php

   <?php

   namespace EtableBundle\Controller;

   use Symfony\Bundle\FrameworkBundle\Controller\Controller;
   use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
   use Symfony\Component\HttpFoundation\RedirectResponse;

   class DefaultController extends Controller
   {
       /**
        * @Route("/", name="home")
        */
       public function indexAction()
       {
           return new RedirectResponse($this->generateUrl('dashboard'));
       }
   }

Pour pouvoir appeler la route ``dashboard``, nous nommons l'action ad hoc dans l controleur :

.. code-block:: php

   <?php

    /**
     * @Route("/dashboard", name="dashboard")
     */
    public function dashboardAction()
    {
        return $this->render('EtableBundle:Dashboard:dashboard.html.twig');
    }



Peupler notre dashboard
-----------------------

Notre dashboard est pour l'instant bien vide. Nous allons le peupler avec les informations suivantes : 

- le nombre de vaches enregistrées dans la base de données ;
- les dernières vaches qui ont été ajoutées à la base de données au cours des 30 derniers jours.

Pour ce faire, nous avons deux possiblités : 

- soit chaque information est calculée dans une seule et même action d'un controleur. Dans ce cas, notre controleur peut être long, ce qui va handicaper sa lisibilité. Un des moyens de résoudre cette longueur est de découper chaque partie en différentes fonctions (l'une va renvoyer le nombre de vaches, une autre va renvoyer la liste des dernières vaches ajoutées, etc.) ;
- nous pouvons aussi appeler un controleur depuis une vue. Nous allons utiliser ce choix: 
    - d'abord parce que, pédagogiquement, cela nous permet de découvrir une nouvelle fonctionnalité de symfony ;
    - ensuite parce que, d'une point de vue de développement, cela nous permettra de ré-utiliser ce controleur dans d'autres zones. Et il est toujours utile de n'avoir à maintenir le moins de code possible.

Nous allons donc : 

- créer des nouvelles actions dans le controleur ;
- appeler ces actions depuis la vue, via la fonction ``render``

.. seealso::

   Embarquer des actions du controleur dans les templates
      `Documentation symfony <http://symfony.com/doc/current/templating/embedding_controllers.html>`_

   Accéder à Doctrine
      Notre base de donnée est gérée par doctrine, qui s'occupe de transformer la base de donnée en objets PHP. 

      La documentation pour accéder à doctrine depuis symfony: http://symfony.com/doc/current/doctrine.html#querying-for-objects

   Effectuer des requêtes Doctrine avec le langage DQL
      Doctrine propose son propre langage de requête, très proche de SQL. Nous allons effectuer nos requêtes à l'aide de ce langage. 

      Documentation de DQL : http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/dql-doctrine-query-language.html

Le code du controleur devient donc le suivant :

.. literalinclude:: _static/code/DashboardController.php.1
   :language: php

Le template dans ``ÈtableBundle/Resources/views/dashboard.html.twig`` est modifié pour appeler les controleurs :

.. literalinclude:: _static/code/dashboard.html.twig.1
   :language: html+jinja

Nous créons également deux templates. Le premier est très simple et va afficher le nombre de vaches :

.. literalinclude:: _static/code/count_cows.html.twig.1
   :language: html+jinja

Le second va permettre de lister les vaches :

.. literalinclude:: _static/code/last_cows.html.twig.1
   :language: html+jinja


Des messages pour les utilisateurs
----------------------------------

Maintenant que nous avons peuplé notre "dashboard", retournons sur les controleurs de notre CRUD. Ils ont été auto-générés pour nous, et nous n'y avons pas touché. Nous allons maintenant y apporter quelques améliorations.

Nous voudrions que les utilisateurs soient prévenus qu'un enregistrement a été effectué correctement. Concrètement,

- un message "<Nom de la vache> a été enregistrée dans l'étable" doit s'afficher lorsqu'une vache a été enregistrée ;
- un message "Les données de <Nom de la vache> ont été modifiées" doit s'afficher lorsqu'une vache a été modifiée avec succès.

Pour cela, nous allons utiliser des **messages flash**. Ces messages sont enregistrés dans la session PHP; ils restent donc enregistrés même si l'utilisateur change de page. Les templates les affiche et, après les avoir affiché, les suppriment de la mémoire.

Symfony apporte cette fonctionnalité dans les controleurs, au moyen d'une fonction: ``addFlash`` dont voici la signature :

.. code-block:: php

    /**
     * Adds a flash message to the current session for type.
     *
     * @param string $type    The type
     * @param string $message The message
     *
     * @throws \LogicException
     */
    protected function addFlash($type, $message)

Le **type** donne une indication sur le type de message. Il s'agit généralement une indication sur le niveau d'alerte.

Pour ajoutons nos messages, nous devons donc avoir deux actions :

- ajouter les messages flash dans le controlleur, si les actions ont réussi ;
- afficher les messages flash dans les templates. Nous allons l'ajouter de telle sorte qu'il soit visible par tous les templates, nous allons donc travailler sur ``base.html.twig``.

Modification de ``CowController``:

.. literalinclude:: _static/code/CowController.php.1
   :language: php
   :emphasize-lines: 51-55,96-100

Modification de ``base.html.twig`` :

.. literalinclude:: _static/code/base.html.twig.1
   :language: html+jinja
   :emphasize-lines: 44-57

.. figure:: _static/add_flash_ajout_vache_non_css.png

   Un message flash avec très peu de mise en forme.

.. seealso::

   La documentation des messages flash
      http://symfony.com/doc/current/controller.html#flash-messages

   Les éléments CSS et javascripts boostrap utilisés dans le code
      - Les **helper classes** : http://getbootstrap.com/css/#helper-classes
      - Les **alert** : http://getbootstrap.com/javascript/#alerts


Un peu de mise en forme
=======================

Dans notre application, nous sommes maintenant capable de visionner les différentes vaches, d'en créer de nouvelles, et de modifier les existantes. Nous disposons également de quelques statistiques. 

Voyons maintenant comment ajouter quelques éléments de mise en forme à l'aide de feuilles de styles CSS.

Pour ajouter des *assets* (le nom qui est donné aux feuilles de styles CSS, aux images, etc.), twig dispose d'une fonction ``asset``. Cette fonction va créer le chemin vers les assets, qui sont normalement contenues dans le répertoire ``web`` (répertoire public).

.. note::

   La gestion des *assets* est à la croisée des chemins. Dans les anciennes version de symfony, un bundle spécifique, nommé ``Assetic`` `était utilisé pour gérer les assets en pur PHP <http://symfony.com/doc/current/assetic.html>`_.

   Ce bundle a été enlevé désormais; et d'autres outils que PHP sont privilégiées. Les développeurs de Symfony `ònt annoncé récemment avoir publié un outil en nodejs <http://symfony.com/blog/introducing-webpack-encore-for-asset-management>`_ qui s'appuie sur `webpack <https://webpack.js.org>`_ qui s'intègre à symfony: `Èncore <http://symfony.com/doc/current/frontend.html>`_

   Lors de cette formation, nous allons nous en tenir à l'ancienne méthode, la plus simple, qui consiste à simplement enregistrer nos feuilles de style CSS dans le répertoire public. 

Nous allons donc :

- créer une feuille de style, qui sera enregistrée dans ``web/css/etable.css`` ;
- charger cette feuille de style dans notre template de base, ``base.html.twig`` ;
- ajouter quelques styles.


Pour charger la feuille de style, nous allons utiliser la fonction ``asset`` de twig, qui `a la signature suivante <http://symfony.com/doc/current/reference/twig_reference.html#asset>`_ : 

.. code-block:: html+jinja

   {{ asset(path, packageName = null) }}

Voici notre feuille de style, très minimale :

.. code-block:: css

   # Le contenu est collé aux bords de la page. Nous allons l'éloigner quelque peu. Cela nécessitera d'ajouter également une balise `div` dans le template.
   div#etable_content {
     margin: 1.5em;
   }

   # Mise en forme des messages flash
   div.flash-messages > div {
     width: 70%;
     padding: 1.5em 2.5em;
     margin: 1.5em auto;
   }

   div.flash-message > div p {
     # pour que les messages flash apparaissent centrés verticalement, nous devons supprimer les marges des balises p (il existe d'autres techniques)
     margin: 0;
   }


Voici les modifications à apporter dans ``base.html.twig`` :

.. code-block:: html+jinja

   <!DOCTYPE html>
   <html>
       <head>
           
           <!--   ..... le code reste identique..... -->

           <!-- ajout d'un lien vers la feuille de style -->
           <link rel="stylesheet" href="{{ asset('css/etable.css') }}" >
       </head>
       <body>
           
           <!--   ..... le code reste identique..... -->

           <!--  on ajoute une balise div autour du contenu (block body) pour la mise en forme -->
           <div id="etable_content">
           {% block body %}{% endblock %}
           </div>
           
           <!--   ..... le code reste identique..... -->

       </body>
   </html>


Sécuriser les accès
===================

Nous voudrions sécuriser notre application :

- les données de l'étable doivent être protégées par mot de passe ;
- seuls certains utilisateurs peuvent ajouter des vaches ;

Nous allons donc :

- modifier la configuration de l'application pour permettre l'authentification, et ajouter les utilisateurs, un mot de passe et les droits ;
- modifier la configuration de l'application pour demander l'authentification ;
- puis modifier le code pour ne permettre la création de vache qu'aux utilisateurs ayants les droits requis.

Dans un premier temps, nous allons nous contenter de la protection 'HTTP Basic': c'est le navigateur qui va afficher une boîte de dialogue pour demander le nom d'utilisateur et le mot de passe. 

Modification de la configuration
--------------------------------

La configuration de la sécurité de base demande l'ajout de quelques lignes :

.. code-block:: yaml

   security:
       # Ici, on indique comment sont chargés les utilisateurs (les 'providers')
       providers:
           in_memory:
               # nous définissons ici un chargement des utilisateurs depuis ce fichiers de 
               # configuration. Ils sont décrits sous la clé 'users'
               memory: 
                   users:
                       julien:
                           password: udos
                           # on définit quelques roles  l'utilisateur.
                           roles: ['ROLE_USER', 'ROLE_COW_CREATOR']
                       robert:
                           password: robert_
                           roles: ['ROLE_USER']

       # les mots de passe peuvent être hashés, de manière à ne pas pouvoir être lus. 
       # ici, on se passera du hashing et on utilisateur le texte simple
       encoders:
           Symfony\Component\Security\Core\User\User: plaintext
  
       firewalls:
           # disables authentication for assets and the profiler, adapt it according to your needs
           dev:
               pattern: ^/(_(profiler|wdt)|css|images|js)/
               security: false

           main:
               # si on laisse cette clé, on permet d'utiliser le site de manière anonyme
               # anonymous: ~
          
               # on active l'authentification via http_basic
               http_basic: ~
               

.. tip::
   Il est possible de définir que certaines routes (par exemple, celles qui commencent par admin) demande une authentification, ou un rôle particulier :

   .. code-block:: yaml

   security:
      access_control:
         - { path: '^/admin', roles: 'ROLE_ADMIN' }

.. seealso::
   Le chapitre sur la sécurité de la documentation de symfony
      https://symfony.com/doc/current/security.html

Sécuriser les controleurs
-------------------------

La sécurisation des controleurs demande l'ajout d'une ligne :

.. code-block:: php

    // EtableBundle/Controller/CowController.php

    public function newAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_COW_CREATOR', null, 
            "Vous n'êtes pas autorisé à ajouter une vache !");
    
        // ...
    }

Adapter les templates
---------------------

Pour éviter des surprises à vos utilisateurs, nous n'allons afficher les boutons que si ils sont en mesure d'exécuter l'action qui y est liée. 

Nous allons donc ajouter une condition autour des boutons, qui va utiliser la fonction ``is_granted`` :

.. code-block:: html+jinja

   {% if is_granted('ROLE_COW_CREATOR') %}
   <!-- votre code ici -->
   {% endif %}


Pour aller plus loin
--------------------

La sécurité est un sujet très vaste :

- Vous voudrez sûrement charger les utilisateurs depuis la base de donnée. `Ceci est possible depuis la configuration de Symfony <https://symfony.com/doc/current/security/entity_provider.html>`_.
- Un bundle tiers permet la création de comptes sur l'application: `FOSUserBundle <https://symfony.com/doc/master/bundles/FOSUserBundle/index.html>`_. C'est certainement un must pour ce genre de fonctionnalités.
- Vous pourrez aussi créer votre propre formulaire de login: il ne demandera qu'une vue, un controleur, et quelques lignes de configuration. `La procédure est décrite dans la documentation de Symfony <http://symfony.com/doc/current/security/form_login_setup.html>`_.

La documentation de Symfony `pourra probablement répondre à vos autres questions <http://symfony.com/doc/current/security.html#learn-more>`_.


Exercices
=========

Après avoir créé les vaches, il faudra traiter la partie "traite" à notre application.

Une traite est caractérisée par : 

- sa date et son heure ;
- le volume de lait récolté, qui est exprimé en litre mais peut être précis au centilitre près ;
- la manière dont la vache a été traite, qui peut être :
    - la trayeuse ;
    - à la main ;
    - la trayeuse, en extérieur (quand les vaches ne reviennent pas à l'étable); 
- chaque traite doit, évidemment, être associée à une vache. 

La traite sera donc une classe particulière. Vous allez également avoir besoin de documenter une association: `a documentation de symfony <https://symfony.com/doc/current/doctrine/associations.html>`_ et `celle de doctrine <http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/association-mapping.html>`_ sont là pour vous aider.

Vous pourrez aussi ajouter un lien vers le formulaire de traite dans le menu.

Seules les personnes qui ont le rôle "ROLE_COW_MILKING" pourront ajouter une traite.

Dans le dashboard, ajoutez également le nombre de litres de lait par jour qui ont été produits, pour les cinq derniers jours. Vous devrez, pour cela, utilisez une requête d'aggrégation qui est disponible en DQL: ``SELECT SUM(t.volume), datetime FROM votre_entite_milk t GROUP BY datetime``. Des exemples `sont listés dans la documentation <http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/dql-doctrine-query-language.html>`_.





.. rubric:: Footnotes

.. [#] Elle est ici mise en évidence avec la commande ``tree -L 3``
.. [#] http://sqliteman.yarpen.cz/ Installation sous debian avec ``sudo apt install sqliteman``
