.. Apprendre symfony documentation master file, created by
   sphinx-quickstart on Wed Jun 21 11:29:06 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Apprendre symfony
#################

Cette formation vise à faire découvrir les fonctions basiques de Symfony. 

Elle a été dispensée par `Champs-Libres <https://www.champs-libres.coop>`_. Avec un public de développeurs aguerris, une durée d'une journée devrait être suffisante, à condition d'avancer relativement vite. 



Contents:

.. toctree::
   :maxdepth: 2

   Premier projet <premier-projet.rst>
   Les services <services.rst>

On se concentre sur...
######################

.. toctree::
   :maxdepth: 2

   Le moteur de template twig <twig.rst>
   Doctrine, l'essentiel <doctrine.rst>

Sources de cette formation
##########################

Cette formation est accompagnées d'une présentation. Les sources de cette formations sont disponibles `sur le dépôt dédié <https://framagit.org/champs-libres/formation_symfony>`_. Elles sont compilées automatiquement et disponibles en ligne :

- `le manuel <https://champs-libres.frama.io/formation_symfony/doc/index.html>`_
- `les slides <https://champs-libres.frama.io/formation_symfony/slides/index.html>`_

Les sources de cette formations sont placées sous licence GFDL.

Indices and tables
##################

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


