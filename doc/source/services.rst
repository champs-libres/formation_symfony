Les services en Symfony
***********************

Les applications écrites en symfony utilisent abondamment des objets. Pour envoyer un email, résoudre une route, ou même accéder à la base de donnée, des objets qui remplissent des petites tâches, mais bien découpées, sont appelés.

En Symfony, ces objets sont appelés services, et naissent au sein d'un objet appelé **service container**. Si vous avez accès au container, vous pouvez avoir accès à tous les services qu'il contient.

Exemple :

.. code-block:: php

   // avoir accès à l'objet doctrine
   $container->get('doctrine.orm')->getEntityManager();

   // avoir accès aux messages flash
   $container->get('session')->getMessageBagh()->addFlashMessage('notice', 'Bravo, vous suivez bien cette formation!');

Quelques identifiants de services couramment utilisés :

=============================== =======================================================================
Service ID                      Class name
=============================== =======================================================================
doctrine                        ``Doctrine\Bundle\DoctrineBundle\Registry``
filesystem                      ``Symfony\Component\Filesystem\Filesystem``
form.factory                    ``Symfony\Component\Form\FormFactory``
logger                          ``Symfony\Bridge\Monolog\Logger``
request_stack                   ``Symfony\Component\HttpFoundation\RequestStack``
router                          ``Symfony\Bundle\FrameworkBundle\Routing\Router``
security.authorization_checker  ``Symfony\Component\Security\Core\Authorization\AuthorizationChecker``
security.password_encoder       ``Symfony\Component\Security\Core\Encoder\UserPasswordEncoder``
session                         ``Symfony\Component\HttpFoundation\Session\Session``
translator                      ``Symfony\Component\Translation\DataCollectorTranslator``
twig                            ``Twig_Environment``
validator                       ``Symfony\Component\Validator\Validator\ValidatorInterface``
=============================== =======================================================================

L'injection de dépendance
=========================

Le **service container** veille aussi à l'injection de dépendance. Il va donner naissance aux services en s'assurant que ces derniers reçoivent les paramètres adéquats à leur cycle de vie. 

Un exemple, dans la vie réelle, est celui du marchand de glace. Pour vendre une glace, le marchand doit rassembler un cornet, et un certain nombre de boule de glace. Sa carte définit la manière dont les boules de glaces sont assemblées: les *dames blanches* comptent trois boules, du chocolat fondu et de la crème chantilly; les *banana splits* y ajoutent une banane, etc.

Le.la marchand.e de glace est donc un injecteur de dépendance: il s'assure que les glaces sont bien constituées pour répondre à la demande du client. La carte est sa configuration.

L'injecteur de dépendance joue le même rôle: vous allez définir une configuration (= la carte) qui précise les dépendances nécessaires. Et l'injecteur de dépenance s'assurera que, au moment où il vous fournira le service que vous avez appelé par votre identifiant unique, il soit constitué selon la configuration requise.

Ainsi, par exemple, l'objet qui gère l'authentification reçoit en paramètre le service qui gère les sessions. Et pour pouvoir authentifier l'utilisateur, ce dernier va recevoir en paramètre un objet qui permet de charger les utilisateurs, depuis une base de donnée ou un fichier de configuration. Si vos utilisateurs sont stockés en base de donnée, l'objet qui vous permet de charger les utilisateurs doit recevoir à son tour un ``EntityRepository`` qui permet de faire une requête SQL parmi les utilisateurs, etc.

Notre premier service: un moteur de recherche
=============================================

Nous allons créer un moteur de recherche pour nos vaches.

Il permettra de recherche des vaches par nom (ou partie de leur nom) et par date de naissance. **Mais** ce moteur de recherche doit être intelligent: il ne proposera qu'un seul champ, et sera capable d'interpréter les dates qui seraient entrées dans ce champ. Pour l'instant, il doit uniquement interpréter les dates au format "dd/mm/yyyy". 

Son code sera suffisamment complexe pour qu'il soit justifié d'être créé dans un service, plutôt que d'être créé dans un controleur. Un deuxième avantage de dédier ce code à un service serait qu'il pourra être utilisé à différents endroits du code.

Nous allons donc procéder comme suit :

- créer une classe de recherche ;
- déclarer cette classe comme service dans la configuration ;
- déclarer un controlleur et son template, qui va effectuer l'action de recherche en appelant le service pré-défini ;
- et, pour faire les choses bien, ajouter un formulaire de recherche dans la barre de navigation.

.. warning::

   Attention au comportement bizarre des recherches par date :

   - l'objet ``DateTime`` contient obligatoirement une heure. Lorsque vous ferez une recherche par date de naissance, il est fort probable que la date ne corresponde pas, vu que l'heure qui sera contenue par l'objet ``DateTime`` ne correspondra pas à l'heure enregistrée dans la base de donnée ;
   - dès lors, vous devrez faire une recherche ``BETWEEN``, entre l'intervalle de la date recherchée moins un jour, et la date recherchée + 1 jour, mais...
   - si vous ajouter ou retranchez un jour à votre date en utilisation la fonction ``DateTime::sub`` et ``DateTime::add``, vous allez obtenir une recherche sur la... date actuelle dans la requête ! En effet, ces deux fonctions modifient la date, ils n'en renvoient pas une nouvelle.
   - la solution est donc de cloner la date **puis** d'y appliquer la fonction sub !

   Voici la requête qui fonctionne : 

   .. code-block:: php

      <?php
       /**
        * search by date of birth
        * 
        * @param string $query
        * @return \EtableBundle\Entity\Cow[]
        * @throws \RuntimeException
        */
       protected function searchByDateOfBirth($query)
       {
           $result = \preg_match($this->patternDate, $query, $matches);
           
           if ($result === 1) {
               $date = \DateTime::createFromFormat('d/m/Y', $matches[0]);
               
               if ($date === false) {
                   throw new \RuntimeException("The date does not match "
                       . "expected form");
               }
               
               return $this->em->createQuery("SELECT c FROM EtableBundle:Cow c "
                   . "WHERE c.birthdate BETWEEN :date_from AND :date_to ")
                   ->setParameter('date_from', (clone $date)->sub(new \DateInterval('P1D')))
                   ->setParameter('date_to'  , (clone $date)->add(new \DateInterval('P1D')))
                   ->getResult();
           } else {
               return array();
           }
       }

Classe de recherche et configuration comme service
--------------------------------------------------

.. literalinclude:: _static/code/CowSearch.php.1
   :language: php

Notre classe a pour dépendance un ``EntityManagerInterface``. Nous allons donc le lui fournir dans la configuration :

.. literalinclude:: _static/code/services.yml.1
   :language: yaml

Notre controleur et son template
--------------------------------

.. literalinclude:: _static/code/SearchController.php.1
   :language: php

.. literalinclude:: _static/code/search.html.twig.1
   :language: html+jinja


La barre de navigation
----------------------

.. code-block:: html

   <nav class="navbar navbar-default">
   <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{ path('home') }}">Mon étable</a>
    </div>
   
   <!-- on ajoute ici le formulaire de recherche --> 
   <form class="navbar-form navbar-right " action="{{ path('search') }}">
      <div class="form-group">
        <input type="text" name="q" class="form-control" placeholder="Search">
      </div>
      <button type="submit" class="btn btn-default">Submit</button>
    </form>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active">
            <a href="{{ path('cow_index') }}">Nos vaches<span class="sr-only">(current)</span></a>
        </li>
        <li><a href="#">Une autre action plus tard</a></li>
      </ul>
      
    </div><!-- /.navbar-collapse -->
   </div><!-- /.container-fluid -->
   </nav>

