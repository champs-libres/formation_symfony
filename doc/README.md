Manuel de la formation "Apprendre Symfony"
==========================================

Compiler le manuel en html
---------------------------


0. (prérequis) Installer Sphinx (sous debian/ubuntu & friends: `pip3 install Sphinx`)
1. `make html`

Le résultat est dans le dossier `build/html`


