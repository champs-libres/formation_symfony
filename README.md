Formation "Découvrir symfony"
=============================

Ce dépôt contient la matière nécessaire à la formation "Découvrir symfony". 

Et notamment :

- Le répertoire `doc` contient le manuel nécessaire. Il est mis à disposition à l'adresse suivante : https://champs-libres.frama.io/formation_symfony/doc/index.html
- le répertoire `src` contient l'application symfony. Il est possible de retracer l'historique de l'application en utilisant l'historique de git ;
- le répertoire `slides` contient la présentation.

Cette formation est disponible sous licence [GFDL](./LICENSE.md).

Une version compilée est disponible en ligne :

- le manuel : https://champs-libres.frama.io/formation_symfony/doc/index.html
- les slides : https://champs-libres.frama.io/formation_symfony_slides/index.html


