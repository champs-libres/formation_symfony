<?php

namespace EtableBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
// pour pouvoir utiliser la validation, nous importons la classe nécessaire
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Cow
 *
 * @ORM\Table(name="cow")
 * @ORM\Entity(repositoryClass="EtableBundle\Repository\CowRepository")
 */
class Cow
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     * @Assert\NotNull()
     * @Assert\Length(
     *      min = 3,
     *      minMessage = "Le nom de la vache doit contenir au moins {{ limit }} trois caractères."
     *  )
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="race", type="string", length=50, nullable=true)
     */
    private $race;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthdate", type="date")
     */
    private $birthdate;

    /**
     * @var string
     *
     * @ORM\Column(name="afscaid", type="string", length=11, unique=true)
     * @Assert\Regex(
     *      pattern = "/[a-zA-Z]{2}[0-9]{9}/",
     *      message = "Le code semble incorrect, désolé..."
     *  )
     */
    private $afscaid;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=25, nullable=true)
     */
    private $color;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datecreation", type="datetime")
     */
    private $datecreation;
    
    const RACE_BBB       = 'bbb';
    const RACE_HOLSTEIN  = 'holstein';
    const RACE_ABONDANCE = 'abondance';
    const RACE_LIMOUSINE = 'limousine';
    const RACE_NORMANDE  = 'normande';

    const COLOR_WHITE  = 'white';
    const COLOR_BLACK_WHITE = 'black_white';
    const COLOR_BROWN   = 'brown';
    const COLOR_BLACK   = 'black';
    
    public function __construct()
    {
        $this->datecreation = new \DateTime('now');
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Cow
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set race
     *
     * @param string $race
     *
     * @return Cow
     */
    public function setRace($race)
    {
        $this->race = $race;

        return $this;
    }

    /**
     * Get race
     *
     * @return string
     */
    public function getRace()
    {
        return $this->race;
    }

    /**
     * Set birthdate
     *
     * @param \DateTime $birthdate
     *
     * @return Cow
     */
    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    /**
     * Get birthdate
     *
     * @return \DateTime
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * Set afscaid
     *
     * @param string $afscaid
     *
     * @return Cow
     */
    public function setAfscaid($afscaid)
    {
        $this->afscaid = $afscaid;

        return $this;
    }

    /**
     * Get afscaid
     *
     * @return string
     */
    public function getAfscaid()
    {
        return $this->afscaid;
    }

    /**
     * Set color
     *
     * @param string $color
     *
     * @return Cow
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set datecreation
     *
     * @param \DateTime $datecreation
     *
     * @return Cow
     */
    public function setDatecreation($datecreation)
    {
        $this->datecreation = $datecreation;

        return $this;
    }

    /**
     * Get datecreation
     *
     * @return \DateTime
     */
    public function getDatecreation()
    {
        return $this->datecreation;
    }
}

