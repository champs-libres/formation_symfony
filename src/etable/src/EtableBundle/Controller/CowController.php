<?php

namespace EtableBundle\Controller;

use EtableBundle\Entity\Cow;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Cow controller.
 *
 * @Route("cow")
 */
class CowController extends Controller
{
    /**
     * Lists all cow entities.
     *
     * @Route("/", name="cow_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $cows = $em->getRepository('EtableBundle:Cow')->findAll();

        return $this->render('cow/index.html.twig', array(
            'cows' => $cows,
        ));
    }

    /**
     * Creates a new cow entity.
     *
     * @Route("/new", name="cow_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_COW_CREATOR', null, 
            "Vous n'êtes pas autorisé à ajouter une vache !");
        
        $cow = new Cow();
        $form = $this->createForm('EtableBundle\Form\CowType', $cow);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($cow);
            $em->flush();
            
            $this->addFlash('success', sprintf(
                "%s a été enregistrée dans l'étable !",
                $cow->getName()
                ));

            return $this->redirectToRoute('cow_show', array('id' => $cow->getId()));
        }

        return $this->render('cow/new.html.twig', array(
            'cow' => $cow,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a cow entity.
     *
     * @Route("/{id}", name="cow_show")
     * @Method("GET")
     */
    public function showAction(Cow $cow)
    {
        $deleteForm = $this->createDeleteForm($cow);

        return $this->render('cow/show.html.twig', array(
            'cow' => $cow,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing cow entity.
     *
     * @Route("/{id}/edit", name="cow_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Cow $cow)
    {
        $deleteForm = $this->createDeleteForm($cow);
        $editForm = $this->createForm('EtableBundle\Form\CowType', $cow);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            
            $this->addFlash('success', sprintf(
                "Les données de \"%s\" ont été modifiées avec succès !",
                $cow->getName()
                ));

            return $this->redirectToRoute('cow_edit', array('id' => $cow->getId()));
        }

        return $this->render('cow/edit.html.twig', array(
            'cow' => $cow,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a cow entity.
     *
     * @Route("/{id}", name="cow_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Cow $cow)
    {
        $form = $this->createDeleteForm($cow);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($cow);
            $em->flush();
        }

        return $this->redirectToRoute('cow_index');
    }

    /**
     * Creates a form to delete a cow entity.
     *
     * @param Cow $cow The cow entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Cow $cow)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cow_delete', array('id' => $cow->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
