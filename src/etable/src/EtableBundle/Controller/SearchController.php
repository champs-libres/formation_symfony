<?php

namespace EtableBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class SearchController extends Controller
{
    /**
     * @Route("/search", name="search")
     */
    public function searchAction(Request $request)
    {
        /* @var $cowSearch \EtableBundle\Search\CowSearch */
        $cowSearch = $this->get('etable.cow_searcher');
        $query = $request->query->get('q', "");
        
        return $this->render('EtableBundle:Search:search.html.twig', array(
            'cows' => $cowSearch->search($query),
            'query' => $query
        ));
    }

}
