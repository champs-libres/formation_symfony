<?php

namespace EtableBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DashboardController extends Controller
{
    /**
     * @Route("/dashboard", name="dashboard")
     */
    public function dashboardAction()
    {
        return $this->render('EtableBundle:Dashboard:dashboard.html.twig');
    }
    
    /**
     * Show the number of cows registered in the database. 
     * 
     * This action does not have any route, it should be embedded in templates.
     * 
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function countCowsAction()
    {
        $nb = $this->getDoctrine()
           ->getManager()
           ->createQuery('SELECT count(cow.id) FROM EtableBundle:Cow cow')
           ->getSingleScalarResult()
            ;
        
        return $this->render('EtableBundle:Dashboard:count_cows.html.twig', [
            'nb' => $nb
        ]);
    }
    
    /**
     * Show the last cows registered in the database
     * 
     * This action does not have any route, it should be embedded in templates.
     * 
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function lastNewCowsAction()
    {
        $date30days = (new \DateTime())->sub(new \DateInterval('P30D'));
        
        $lastCows = $this->getDoctrine()
            ->getManager()
            ->createQuery('SELECT cow FROM EtableBundle:Cow cow '
                . 'WHERE cow.datecreation > :date '
                . 'ORDER BY cow.datecreation DESC ')
            // on récupère ici la date d'il y a trente jours
            ->setParameter('date', $date30days)
            ->setMaxResults(5)
            ->getResult()
            ;
        
        $nbLastCows = $this->getDoctrine()
            ->getManager()
            ->createQuery('SELECT count(cow) FROM EtableBundle:Cow cow '
                . 'WHERE cow.datecreation > :date ')
            ->setParameter('date', $date30days)
            ->getSingleScalarResult()
            ;
        
        return $this->render('EtableBundle:Dashboard:last_cows.html.twig', [
            'last_cows' => $lastCows,
            'nb_last_cows' => $nbLastCows
        ]);
    }

}
